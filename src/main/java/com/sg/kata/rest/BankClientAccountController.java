package com.sg.kata.rest;

import com.sg.kata.dao.entity.Account;
import com.sg.kata.dao.entity.Operation;
import com.sg.kata.dao.exceptions.InvalidAccountException;
import com.sg.kata.services.BankClientAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * bank client account controller
 */
@RestController
@RequestMapping("/account")
public class BankClientAccountController {

    private final BankClientAccountService bankClientAccountService;

    @Autowired
    public BankClientAccountController(BankClientAccountService bankClientAccountService) {
        this.bankClientAccountService = bankClientAccountService;
    }

    /**
     * get account by id
     *
     * @param accountId - accountId
     * @return ResponseEntity <Account>
     * @throws InvalidAccountException - InvalidAccountException
     */
    @GetMapping("/{accountId}")
    public ResponseEntity<Account> getAccountById(@PathVariable("accountId") String accountId) throws InvalidAccountException {
        return ResponseEntity.ok(bankClientAccountService.getAccountById(accountId));
    }

    @GetMapping("/{accountId}/operations")
    public ResponseEntity<List<Operation>> getAccountOperations(@PathVariable("accountId") String accountId) throws InvalidAccountException {
        return ResponseEntity.ok(bankClientAccountService.getAccountOperations(accountId));
    }

    /**
     * save account
     *
     * @param account - account
     * @return ResponseEntity <Account>
     * @throws InvalidAccountException - InvalidAccountException
     */
    @PostMapping("/save")
    public ResponseEntity<Account> saveAccount(@RequestBody Account account) throws InvalidAccountException {
        return ResponseEntity.ok(bankClientAccountService.saveAccount(account));
    }

    /**
     * deposit money on account
     *
     * @param accountId - accountId
     * @param amount    - amount
     * @return ResponseEntity <Void>
     * @throws InvalidAccountException - InvalidAccountException
     */
    @PostMapping("/{accountId}/deposit")
    public ResponseEntity<Void> depositMoneyOnAccount(@PathVariable("accountId") String accountId, @RequestBody BigDecimal amount) throws InvalidAccountException {
        bankClientAccountService.depositMoneyOnAccount(accountId, amount);
        return ResponseEntity.ok().build();
    }

    /**
     * withdrawal money on account
     *
     * @param accountId - accountId
     * @param amount    - amount
     * @return ResponseEntity <Void>
     * @throws InvalidAccountException - InvalidAccountException
     */
    @PostMapping("/{accountId}/withdrawal")
    public ResponseEntity<Void> withdrawalMoneyOnAccount(@PathVariable("accountId") String accountId, @RequestBody BigDecimal amount) throws InvalidAccountException {
        bankClientAccountService.withdrawalMoneyOnAccount(accountId, amount);
        return ResponseEntity.ok().build();
    }

}
