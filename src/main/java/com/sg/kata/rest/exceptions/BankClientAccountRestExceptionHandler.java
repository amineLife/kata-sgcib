package com.sg.kata.rest.exceptions;

import com.sg.kata.dao.exceptions.InvalidAccountException;
import com.sg.kata.rest.exceptions.dto.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * bank client account rest exception handler
 */
@RestControllerAdvice
public class BankClientAccountRestExceptionHandler {

    @ExceptionHandler(value = InvalidAccountException.class)
    public ResponseEntity<ApiError> invalidAccountException(InvalidAccountException exception) {
        return ResponseEntity.ok(
            new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage())
        );
    }

}
