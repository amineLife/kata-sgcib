package com.sg.kata.dao.enums;

/**
 * bank client account operation direction
 */
public enum DirectionType {

    DEPOSIT,
    WITHDRAWAL

}
