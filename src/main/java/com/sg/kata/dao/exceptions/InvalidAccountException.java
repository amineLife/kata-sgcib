package com.sg.kata.dao.exceptions;

/**
 * bank client invalid account exception
 */
public class InvalidAccountException extends Exception {

    public InvalidAccountException(String message) {
        super(message);
    }

}
