package com.sg.kata.dao;

import com.sg.kata.dao.entity.Account;
import com.sg.kata.dao.exceptions.InvalidAccountException;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.*;

/**
 * bank account local repository
 */
@Repository
public class BankClientAccountDao {

    private final Map<String, Account> bankClientsAccounts;

    public BankClientAccountDao() {
        bankClientsAccounts = new HashMap<>();
    }

    /**
     * get account by id
     *
     * @param accountId - accountId
     * @return Account
     */
    public Account getAccountById(String accountId) {
        return bankClientsAccounts.get(accountId);
    }

    /**
     * save bank client account
     *
     * @param account - account
     */
    public void saveBankClientAccount(Account account) throws InvalidAccountException {
        Optional<Account> accountOptional = Optional.ofNullable(account);

        if (accountOptional.isPresent()) {
            account.setAccountId(generateBankClientAccountUniqueId(accountOptional.get()));
            account.setOperations(new ArrayList<>());
            account.setBalance(BigDecimal.ZERO);

            bankClientsAccounts.putIfAbsent(
                accountOptional.get().getAccountId(),
                accountOptional.get()
            );
        }
    }

    /**
     * generate bank client account unique id
     *
     * @param account - account
     * @return String
     * @throws InvalidAccountException - InvalidAccountException
     */
    private String generateBankClientAccountUniqueId(Account account) throws InvalidAccountException {
        if (Objects.isNull(account.getFirstName()) || Objects.isNull(account.getLastName())) {
            throw new InvalidAccountException("invalid account information!!!");
        }

        return account.getFirstName().concat("-").concat(account.getLastName());
    }

}
