package com.sg.kata.dao.entity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * bank client account
 */
public class Account {

    private String          accountId;

    private String          firstName;
    private String          lastName;

    private BigDecimal      balance;
    private List<Operation> operations;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    /**
     * add operation to bank account client
     *
     * @param operation - operation
     */
    public void addOperation(Operation operation) {
        Optional.ofNullable(operation)
            .ifPresent(operations::add);
    }

}
