package com.sg.kata.dao.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.sg.kata.dao.enums.DirectionType;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * bank client account operations
 */
public class Operation {

    private DirectionType directionType;
    private BigDecimal    amount;
    private BigDecimal    balance;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime operationDateTime;

    public Operation(DirectionType directionType, BigDecimal amount, BigDecimal balance, LocalDateTime operationDateTime) {
        this.directionType = directionType;
        this.amount = amount;
        this.balance = balance;
        this.operationDateTime = operationDateTime;
    }

    public DirectionType getDirectionType() {
        return directionType;
    }

    public void setDirectionType(DirectionType directionType) {
        this.directionType = directionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public LocalDateTime getOperationDateTime() {
        return operationDateTime;
    }

    public void setOperationDateTime(LocalDateTime operationDateTime) {
        this.operationDateTime = operationDateTime;
    }

}
