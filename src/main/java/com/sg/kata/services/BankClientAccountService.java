package com.sg.kata.services;

import com.sg.kata.dao.BankClientAccountDao;
import com.sg.kata.dao.entity.Account;
import com.sg.kata.dao.entity.Operation;
import com.sg.kata.dao.enums.DirectionType;
import com.sg.kata.dao.exceptions.InvalidAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * bank client account service
 */
@Service
public class BankClientAccountService {

    private final BankClientAccountDao bankClientAccountDao;

    @Autowired
    public BankClientAccountService(BankClientAccountDao bankClientAccountDao) {
        this.bankClientAccountDao = bankClientAccountDao;
    }

    /**
     * save account
     *
     * @param account - account
     * @return Account
     * @throws InvalidAccountException - InvalidAccountException
     */
    public Account saveAccount(Account account) throws InvalidAccountException {
        bankClientAccountDao.saveBankClientAccount(account);
        return bankClientAccountDao.getAccountById(
            account.getFirstName()
                .concat("-")
                .concat(account.getLastName())
        );
    }

    /**
     * deposit money on account
     *
     * @param accountId - accountId
     * @param amount    - amount
     */
    public void depositMoneyOnAccount(String accountId, BigDecimal amount) throws InvalidAccountException {
        Account account = getAccountById(accountId);

        // deposit money on account
        account.setBalance(account.getBalance().add(amount));

        // save operation
        account.addOperation(
            new Operation(
                DirectionType.DEPOSIT,
                amount,
                account.getBalance(),
                LocalDateTime.now()
            )
        );
    }

    /**
     * withdrawal money on account
     *
     * @param accountId - accountId
     * @param amount    - amount
     * @throws InvalidAccountException - InvalidAccountException
     */
    public void withdrawalMoneyOnAccount(String accountId, BigDecimal amount) throws InvalidAccountException {
        Account account = getAccountById(accountId);

        // deposit money on account
        account.setBalance(account.getBalance().subtract(amount));

        // save operation
        account.addOperation(
            new Operation(
                DirectionType.WITHDRAWAL,
                amount,
                account.getBalance(),
                LocalDateTime.now()
            )
        );
    }

    /**
     * get account operations
     *
     * @param accountId - accountId
     * @return List <Operation>
     * @throws InvalidAccountException - InvalidAccountException
     */
    public List<Operation> getAccountOperations(String accountId) throws InvalidAccountException {
        Optional<Account> accountOptional = Optional.ofNullable(
            bankClientAccountDao.getAccountById(accountId)
        );

        if (!accountOptional.isPresent()) {
            throw new InvalidAccountException("invalid account information!!!");
        }

        return accountOptional.get().getOperations();
    }

    /**
     * get account by id
     *
     * @param accountId - accountId
     * @return Account
     */
    public Account getAccountById(String accountId) throws InvalidAccountException {
        Optional<Account> accountOptional = Optional.ofNullable(
            bankClientAccountDao.getAccountById(accountId)
        );

        if (!accountOptional.isPresent()) {
            throw new InvalidAccountException("invalid account information!!!");
        }

        return accountOptional.get();
    }

}
